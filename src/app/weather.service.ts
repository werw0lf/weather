import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  APIKEY = '73fb7b1e78b9789c89eeaf3922361703';

  constructor(private http: HttpClient) { }

  getWeather(city: string) {
    return this.http.get(`http://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${this.APIKEY}&units=metric`);
  }
}
