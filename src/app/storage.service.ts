import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { WeatherService } from './weather.service';
import { take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  cities = new BehaviorSubject<Array<string>>([]);

  constructor(private weatherService: WeatherService) {
    this.loadCities();
  }

  getCity() {
    return localStorage.getItem('city');
  }

  setCity(name: string) {
    return localStorage.setItem('city', name);
  }

  getCities() {
    return this.cities;
  }

  addCity(city: string) {
    return this.checkCity(city).pipe(tap(() => {
      const cities = this.cities.getValue();
      cities.push(city);
      localStorage.setItem('cities', JSON.stringify(cities));
      this.loadCities();
    }));
  }

  deleteCities(indexes: Array<number>) {
    const cities = this.cities.getValue();
    localStorage.setItem('cities', JSON.stringify(cities.filter((_, id) => !indexes.includes(id))));
    this.loadCities();
  }

  private loadCities() {
    this.cities.next(JSON.parse(localStorage.getItem('cities') || '[]'));
  }

  private checkCity(city) {
    return this.weatherService.getWeather(city).pipe(take(1));
  }
}
