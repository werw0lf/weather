import { Component, OnInit, OnDestroy } from '@angular/core';
import { WeatherService } from '../weather.service';
import { tap } from 'rxjs/operators';
import { Observable, interval, Subscription } from 'rxjs';
import { MatCheckboxChange } from '@angular/material/checkbox';
import { StorageService } from '../storage.service';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.css']
})
export class WeatherComponent implements OnInit, OnDestroy {
  city: string;
  updatedAt: Date;
  isLoading: boolean;
  weather: Observable<any>;
  sub: Subscription;
  autoupdate: boolean;

  constructor(private weatherService: WeatherService, private storageService: StorageService) { }

  ngOnInit() {
    this.city = this.storageService.getCity();
    this.autoupdate = !!localStorage.getItem('autoupdate');
    this.getWeather();
  }

  toggleAutoupdate(event: MatCheckboxChange) {
    this.sub && this.sub.unsubscribe();
    localStorage.setItem('autoupdate', event.checked ? '1' : '');
    if (event.checked) {
      this.sub = interval(60000).subscribe(() => this.getWeather());
    }
  }

  ngOnDestroy() {
    this.sub && this.sub.unsubscribe();
  }

  getWeather() {
    this.isLoading = true;
    this.weather = this.weatherService.getWeather(this.city).pipe(
      tap(() => this.isLoading = false),
      tap(() => this.updatedAt = new Date()),
    );
  }

}
