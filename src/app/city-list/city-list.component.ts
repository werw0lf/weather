import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatListOption } from '@angular/material/list';
import { MatSnackBar } from '@angular/material/snack-bar';
import { StorageService } from '../storage.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Component({
  selector: 'app-city-list',
  templateUrl: './city-list.component.html',
  styleUrls: ['./city-list.component.css']
})
export class CityListComponent implements OnInit {
  cities: Observable<Array<string>>;
  editMode: boolean;

  constructor(private router: Router, private storageService: StorageService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.cities = this.storageService.getCities();
  }

  addCity(city: string) {
    this.storageService.addCity(city)
      .pipe(catchError((e) => {
        if (e.status === 404) {
          this.snackBar.open('City not found!', 'X');
        }
        return of(null);
      }))
      .subscribe();
  }

  deleteCities(items: Array<MatListOption>) {
    this.storageService.deleteCities(items.map(item => item.value));
  }

  selectCity(city: string) {
    this.storageService.setCity(city);
    this.router.navigate(['/']);
  }

}
